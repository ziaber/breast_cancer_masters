import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import classification_report
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from operator import add


def plotScores(history, epochs, filepath):
    acc = history.history['binary_accuracy']
    validation_acc = history.history['val_binary_accuracy']

    loss = history.history['loss']
    validation_loss = history.history['val_loss']

    precision = history.history['precision']
    validation_precision = history.history['val_precision']

    recall = history.history['recall']
    validation_recall = history.history['val_recall']

    auc = history.history['auc']
    validation_auc = history.history['val_auc']

    epochs_range = range(epochs)

    plt.figure(figsize=(24, 16), dpi=300)
    plt.subplot(2, 3, 1)
    plt.plot(epochs_range, acc, label='Training Accuracy')
    plt.plot(epochs_range, validation_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 3, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, validation_loss, label='Validation Loss')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Loss')

    plt.subplot(2, 3, 3)
    plt.plot(epochs_range, precision, label='Training Precision')
    plt.plot(epochs_range, validation_precision, label='Validation Precision')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Precision')

    plt.subplot(2, 3, 4)
    plt.plot(epochs_range, recall, label='Training Recall')
    plt.plot(epochs_range, validation_recall, label='Validation Recall')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Recall')

    plt.subplot(2, 3, 5)
    plt.plot(epochs_range, auc, label='Training AUC')
    plt.plot(epochs_range, validation_auc, label='Validation AUC')
    plt.legend(loc='lower right')
    plt.title('Training and Validation AUC')
    # plt.show()
    if filepath:
        plt.savefig(filepath, dpi=300)
    plt.close()


def plotImages(images_arr):
    fig, axes = plt.subplots(1, 5)
    axes = axes.flatten()
    for img, ax in zip(images_arr, axes):
        ax.imshow(img)
        ax.axis('off')
    plt.tight_layout()
    plt.show()


def evaluateAndPrintScores(model, flow):
    print("Scores from scikit: \n")
    y_pred1 = model.predict(flow)
    y_pred = (y_pred1 > 0.5).astype(np.int).flatten()
    y_true = flow.classes
    print(classification_report(y_true, y_pred))

    print("Scores from keras: \n")
    score = model.evaluate(flow)
    print('Loss: ', score[0])
    print('Accuracy: ', score[1])
    print('Precision: ', score[2])
    print('Recall: ', score[3])


def evaluateAndSaveScores(model, flow, filepath, target_names):
    text_file = open(filepath, "w")
    text_file.write("Scores from scikit: \n")
    y_pred1 = model.predict(flow)
    y_pred = (y_pred1 > 0.5).astype(np.int).flatten()
    y_true = flow.classes
    report = classification_report(y_true=y_true, y_pred=y_pred, target_names=target_names, digits=8, output_dict=True)

    fprs = np.linspace(0, 1, 1500)
    fpr_keras, tpr_keras, thresholds_keras = roc_curve(y_true, y_pred1, drop_intermediate=False)
    interpolated_tprs = np.interp(fprs, fpr_keras, tpr_keras)
    interpolated_tprs[0] = 0.0
    auc_keras = auc(fpr_keras, tpr_keras)
    interpolated_auc = auc(fprs, interpolated_tprs)
    print("AUC-ROC value: " + str(auc_keras))
    print("AUC-ROC interpolated value: " + str(interpolated_auc))

    report['fpr'] = fprs
    report['tpr'] = interpolated_tprs
    report['thresholds'] = thresholds_keras
    report['auc'] = auc_keras
    text_file.write(str(report))

    plot_roc_and_save_to_file(fpr_keras, tpr_keras, auc_keras, filepath + "auc.png")

    text_file.write("\nScores from keras: \n")
    score = model.evaluate(flow)
    text_file.write("Loss: " + str(score[0]) + "\nAccuracy: " + str(score[1]) + "\nPrecision: " + str(
        score[2]) + "\nRecall: " + str(score[3]))

    text_file.write("\n\n")
    stringlist = []
    model.summary(print_fn=lambda x: stringlist.append(x))
    short_model_summary = "\n".join(stringlist)
    text_file.write(short_model_summary)

    text_file.close()
    return report


def plot_roc_and_save_to_file(fpr_keras, tpr_keras, auc_keras, filepath):
    plt.figure(dpi=300)
    plt.plot([0, 1], [0, 1], 'k--', label='Random classifier', color='r')
    plt.plot(fpr_keras, tpr_keras, label='ROC (AUC = {:.3f})'.format(auc_keras), color='b')
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc='best')
    if filepath:
        plt.savefig(filepath, dpi=300)
    plt.close()

def plot_summary_roc_and_save_to_file(mean_fpr, mean_tpr, mean_auc, std_auc, tprs_lower, tprs_upper, filepath):
    fig, ax = plt.subplots()
    ax.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',
            label='Random classifier', alpha=.8)
    ax.plot(mean_fpr, mean_tpr, color='b',
            label=r'Mean ROC (AUC = %0.3f $\pm$ %0.3f)' % (mean_auc, std_auc),
            lw=2, alpha=.8)
    ax.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                    label=r'$\pm$ 1 std. dev.')

    ax.set(xlim=[-0.05, 1.05], ylim=[-0.05, 1.05],
           title="Receiver operating characteristic")
    ax.legend(loc="lower right")

    ax.set_xlabel('False positive rate')
    ax.set_ylabel('True positive rate')
    if filepath:
        plt.savefig(filepath, dpi=300)
    plt.close()

def calculateAveragesAndSaveThem(reports, number_of_datasets, filepath, roc_filepath):
    for report in reports:
        print(len(report['fpr']))
        print(len(report['tpr']))

    tprs = [report['tpr'] for report in reports]
    fprs = [report['fpr'] for report in reports]
    thresholds = [report['thresholds'] for report in reports]

    std_tpr = np.std(tprs, axis=0)
    mean_tprs = np.mean(tprs, axis=0)
    mean_tprs[-1] = 1.0
    mean_fprs = fprs[0] #fprs are the same for every fold
    tprs_upper = np.minimum(mean_tprs + std_tpr, 1)
    tprs_lower = np.maximum(mean_tprs - std_tpr, 0)

    mean_auc = auc(mean_fprs, mean_tprs)
    std_auc = np.std([report['auc'] for report in reports])
    final_report = {
        'benign': {
            'precision': sum(map(lambda report: report['benign']['precision'], reports)) / number_of_datasets,
            'recall': sum(map(lambda report: report['benign']['recall'], reports)) / number_of_datasets,
            'f1-score': sum(map(lambda report: report['benign']['f1-score'], reports)) / number_of_datasets,
            'support': sum(map(lambda report: report['benign']['support'], reports)) / number_of_datasets
        },
        'malignant': {
            'precision': sum(map(lambda report: report['malignant']['precision'], reports)) / number_of_datasets,
            'recall': sum(map(lambda report: report['malignant']['recall'], reports)) / number_of_datasets,
            'f1-score': sum(map(lambda report: report['malignant']['f1-score'], reports)) / number_of_datasets,
            'support': sum(map(lambda report: report['malignant']['support'], reports)) / number_of_datasets
        },
        'accuracy': sum(map(lambda report: report['accuracy'], reports)) / number_of_datasets,
        'macro avg': {
            'precision': sum(map(lambda report: report['macro avg']['precision'], reports)) / number_of_datasets,
            'recall': sum(map(lambda report: report['macro avg']['recall'], reports)) / number_of_datasets,
            'f1-score': sum(map(lambda report: report['macro avg']['f1-score'], reports)) / number_of_datasets,
            'support': sum(map(lambda report: report['macro avg']['support'], reports)) / number_of_datasets
        },
        'weighted avg': {
            'precision': sum(map(lambda report: report['weighted avg']['precision'], reports)) / number_of_datasets,
            'recall': sum(map(lambda report: report['weighted avg']['recall'], reports)) / number_of_datasets,
            'f1-score': sum(map(lambda report: report['weighted avg']['f1-score'], reports)) / number_of_datasets,
            'support': sum(map(lambda report: report['weighted avg']['support'], reports)) / number_of_datasets
        },
        'mean_fpr': mean_fprs,
        'mean_tpr': mean_tprs,
        # 'mean_thresholds': np.mean(thresholds, axis=0),
        'mean_auc': mean_auc
    }
    text_file = open(filepath, "w")
    text_file.write(str(final_report))
    text_file.close()
    plot_summary_roc_and_save_to_file(mean_fprs, mean_tprs, mean_auc, std_auc, tprs_lower, tprs_upper, roc_filepath)
