# breast_cancer_masters

Project contains different attention models implementations in Keras
for breast cancer classification.

Implementations:
* CNN - normal CNN network
* CBAM - model from https://arxiv.org/abs/1807.06521
* attention_augmented - model from Attengion Augmented CNN - https://arxiv.org/pdf/1904.09925.pdf
* attention_guided - multi branch attention model from https://arxiv.org/pdf/1801.09927.pdf
