import os

from PIL import Image
from sklearn.model_selection import StratifiedKFold


def resize_and_save_image(full_path_to_file, MAX_WIDTH, train_benign_dir_path, file):
    image = Image.open(full_path_to_file)
    image.thumbnail((MAX_WIDTH, MAX_WIDTH))
    image.save(train_benign_dir_path + file)

if __name__ == "__main__":
    IMG_HEIGHT = 230
    IMG_WIDTH = 350
    MAX_WIDTH = 350
    number_of_splits = 5
    general_split_path = "../../"
    train_benign_dir_paths = []
    train_malignant_dir_paths = []
    test_benign_dir_paths = []
    test_malignant_dir_paths = []


    for split_number in range(number_of_splits):
        base_path = "../../datasets/prepared/k_split/split_" + str(split_number)

        train_benign_dir_path = base_path + "/train/benign/"
        train_benign_dir_paths.append(train_benign_dir_path)
        os.makedirs(os.path.dirname(train_benign_dir_path), exist_ok=True)

        train_malignant_dir_path = base_path + "/train/malignant/"
        train_malignant_dir_paths.append(train_malignant_dir_path)
        os.makedirs(os.path.dirname(train_malignant_dir_path), exist_ok=True)

        test_benign_dir_path = base_path + "/test/benign/"
        test_benign_dir_paths.append(test_benign_dir_path)
        os.makedirs(os.path.dirname(test_benign_dir_path), exist_ok=True)

        test_malignant_dir_path = base_path + "/test/malignant/"
        test_malignant_dir_paths.append(test_malignant_dir_path)
        os.makedirs(os.path.dirname(test_malignant_dir_path), exist_ok=True)

    filepaths = []
    classes = []
    for root, dirs, files in os.walk("../../datasets/BreaKHis_v1/histology_slides/breast"):
        for file in files:
            if ".png" in file:
                full_path_to_file = os.path.join(root, file)
                filepaths.append((full_path_to_file, file))
                if "benign" in full_path_to_file:
                    classes.append(0)
                elif "malignant" in full_path_to_file:
                    classes.append(1)
    skf = StratifiedKFold(n_splits=number_of_splits, shuffle=True)
    split_number = 0
    for train_index, test_index in skf.split(filepaths, classes):
        print("TRAIN_INDEX: ", train_index, "COUNT: ", len(train_index), "TEST_INDEX", test_index, "COUNT: ", len(test_index))
        for index in train_index:
            full_path_to_file, file = filepaths[index]
            if "benign" in full_path_to_file:
                resize_and_save_image(full_path_to_file, MAX_WIDTH,
                                      train_benign_dir_paths[split_number], file)
            elif "malignant" in full_path_to_file:
                resize_and_save_image(full_path_to_file, MAX_WIDTH,
                                      train_malignant_dir_paths[split_number], file)

        for index in test_index:
            full_path_to_file, file = filepaths[index]
            if "benign" in full_path_to_file:
                resize_and_save_image(full_path_to_file, MAX_WIDTH,
                                      test_benign_dir_paths[split_number], file)
            elif "malignant" in full_path_to_file:
                resize_and_save_image(full_path_to_file, MAX_WIDTH,
                                      test_malignant_dir_paths[split_number], file)
        split_number += 1


