import json


def calculateAveragesAndSaveThem(reports, number_of_datasets, filepath):
    final_report = {
        'benign': {
            'precision': sum(map(lambda report: report['benign']['precision'], reports)) / number_of_datasets,
            'recall': sum(map(lambda report: report['benign']['recall'], reports)) / number_of_datasets,
            'f1-score': sum(map(lambda report: report['benign']['f1-score'], reports)) / number_of_datasets,
            'support': sum(map(lambda report: report['benign']['support'], reports)) / number_of_datasets
        },
        'malignant': {
            'precision': sum(map(lambda report: report['malignant']['precision'], reports)) / number_of_datasets,
            'recall': sum(map(lambda report: report['malignant']['recall'], reports)) / number_of_datasets,
            'f1-score': sum(map(lambda report: report['malignant']['f1-score'], reports)) / number_of_datasets,
            'support': sum(map(lambda report: report['malignant']['support'], reports)) / number_of_datasets
        },
        'accuracy': sum(map(lambda report: report['accuracy'], reports)) / number_of_datasets,
        'macro avg': {
            'precision': sum(map(lambda report: report['macro avg']['precision'], reports)) / number_of_datasets,
            'recall': sum(map(lambda report: report['macro avg']['recall'], reports)) / number_of_datasets,
            'f1-score': sum(map(lambda report: report['macro avg']['f1-score'], reports)) / number_of_datasets,
            'support': sum(map(lambda report: report['macro avg']['support'], reports)) / number_of_datasets
        },
        'weighted avg': {
            'precision': sum(map(lambda report: report['weighted avg']['precision'], reports)) / number_of_datasets,
            'recall': sum(map(lambda report: report['weighted avg']['recall'], reports)) / number_of_datasets,
            'f1-score': sum(map(lambda report: report['weighted avg']['f1-score'], reports)) / number_of_datasets,
            'support': sum(map(lambda report: report['weighted avg']['support'], reports)) / number_of_datasets,
        }
    }
    text_file = open(filepath, "w")
    text_file.write(str(final_report))
    text_file.close()


reports = []
for i in range(5):
    with open('16_32_64_batch_32_fc_512_epochs_50_dataset_' + str(i) + '_scores.txt') as json_file:
        json_file.readline()
        result = json_file.readline()
        # print(result)
        data = json.loads(result)

        print(data)
        reports.append(data)
calculateAveragesAndSaveThem(reports, 5, 'final_report.json')
