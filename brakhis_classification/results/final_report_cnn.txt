{'benign':
{'precision' to jest NPV: 0.8801399596186983,
'recall' specificity: 0.7963709677419356,
'f1-score': 0.833661718739991,
'support': 496.0},

'malignant':
{'precision' to jest precision: 0.911372137517288,
'recall' sensitivity: 0.947878402118288,
'f1-score' to jest F1-score: 0.928759972913117,
'support': 1085.8},

'accuracy': 0.9003684716821357,

'macro avg': {
'precision': 0.8957560485679931,
'recall': 0.8721246849301117,
'f1-score': 0.881210845826554,
'support': 1581.8},

'weighted avg': {
'precision': 0.9015816574066063,
'recall': 0.9003684716821357,
'f1-score': 0.8989405108407593,
 'support': 1581.8},

 'mean_fpr': array([0.00000000e+00, 6.67111408e-04, 1.33422282e-03, ...,
       9.98665777e-01, 9.99332889e-01, 1.00000000e+00]),

'mean_tpr': array([0.       , 0.3407706, 0.3407706, ..., 1.       , 1.       ,
       1.       ]),

'mean_auc': 0.9594963844600541}
